# Sección 3: Más componentes primitivos (_FlatList_).

Otro componente que nos ofrece `react-native` es esta lista gráfica que nos permite interpretar datos desde un arreglo o diccionario de datos lógico, o sea,
es un componente que nos permite mostrar ante la interfaz de usuario una lista de datos de manera **adecuada**

```javascript
return (
        <FlatList keyExtractor = {amix => amix.friend}
            data = {amixes} 
            renderItem = {({item}) => {
                return <Text style={estilos.textStyle}>Amigo: {item.friend} - Edad: {item.age}</Text>;
            }}
        />
    );
```
En el código superior se muestra la sintaxis del componente y algunas propiedades del mismo.

**Notas:**
- El atributo `KeyExtractor` genera un identificador para cada uno de los objetos dentro del arreglo de datos (este debe de ser _string_): esto es para que
    la lista no se renderice toda al modificar un solo elemento de la misma.
- El atributo `renderItem` se encarga de dar una estructura grafica a la lista con mas componentes de 'react-native' a cada uno de los objetos existentes en el arreglo de datos.
- El atributo `data` obtiene el arreglo de datos a enlistar. 


